import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_bloc/src/form_bloc.dart';
import 'package:form_bloc/src/model.dart';
import 'package:provider/provider.dart';
import 'package:pattern_formatter/pattern_formatter.dart';

class Formulario extends StatelessWidget {
  final Modelo modelo = Modelo(cadena: "Marcos", entero: 0, decimal: 0.00);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<FormBloc>(
      builder: (_) => FormBloc(modelo),
      child: Consumer<FormBloc>(
        builder: (_, bloc, __) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                controller: bloc.cadenaController,
                decoration: InputDecoration(errorText: bloc.cadenaErrorText),
              ),
              TextFormField(
                controller: bloc.enteroController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(errorText: bloc.enteroErrorText),
                inputFormatters: [ThousandsFormatter()],
              ),
              TextFormField(
                controller: bloc.decimalController,
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                decoration: InputDecoration(errorText: bloc.decimalErrorText),
                inputFormatters: [ThousandsFormatter(allowFraction: true)],
              ),
              RaisedButton(
                color: Colors.blue,
                child: Text("Submit"),
                onPressed: bloc.submit,
              )
            ],
          );
        },
      ),
    );
  }
}
