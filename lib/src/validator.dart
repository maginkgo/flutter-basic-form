class Validator {
  String cadenaValidator(String value) {
    if (value.isEmpty) return "Campo requerido";
    if (value.length < 6) return "Muy corta";
    return null;
  }

  String enteroValidator(String value) {
    if (value.isEmpty) return "Campo requerido";
    return null;
  }

  String decimalValidator(String value) {
    if (value.isEmpty) return "Campo requerido";
    return null;
  }
}