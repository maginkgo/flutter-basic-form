import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:form_bloc/src/model.dart';
import 'package:form_bloc/src/validator.dart';

class FormBloc extends ChangeNotifier with Validator {
  Modelo modelo;

  TextEditingController cadenaController = TextEditingController();
  TextEditingController enteroController = TextEditingController();
  TextEditingController decimalController = TextEditingController();

  String cadenaErrorText;
  String enteroErrorText;
  String decimalErrorText;

  Function submit;

  bool isValidForm() {
    return [
      cadenaErrorText,
      enteroErrorText,
      decimalErrorText,
    ].every((e) => e == null);
  }

  FormBloc(this.modelo) {
    cadenaController.text = modelo.cadena;
    enteroController.text = modelo.entero.toString();
    decimalController.text = modelo.decimal.toString();

    cadenaController.addListener(listener);
    enteroController.addListener(listener);
    decimalController.addListener(listener);
  }

  void listener() {
    cadenaErrorText = cadenaValidator(cadenaController.text);
    enteroErrorText = enteroValidator(enteroController.text);
    decimalErrorText = decimalValidator(decimalController.text);

    submit = isValidForm() ? save : null;

    notifyListeners();
  }

  void save() {
    print("Guardar Modelo");
  }
}
