import 'package:flutter/material.dart';
import 'package:form_bloc/src/formulario.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Formulario(),
      ),
    );
  }
}

